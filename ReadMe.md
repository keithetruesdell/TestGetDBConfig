### Objective  
The objective of this is just to try to get the data from a config file for the database connection.

The situation is where one would have a C# executable on a server that would be executed by the webpage.

The security standards are that we don't want the connection string in the SVN/GIT repository at all (besides these "test" ones that are dummy).

That also includes having the connection string in the code that will be in the repository (SVN/GIT).

The security standards and common sense also say we shouldn't have the config file with the connection string in a directory accessible via the web.  

Only something that would be accessible by the EXE.

The XML config file is the format provided below....

```xml
            <configuration>
	            <connectionStrings>
		            <add name="webDB" connectionString="Data Source=101.1.1.1;Initial Catalog=myDb;User ID=mySecureID;Password=password" />
	            </connectionStrings>
            </configuration>
```

**NOTE:** Change the Data Source string to what you need it, mine was changed for example purposes.  
**NOTE:** Change the "name="webDB"" part of the config file to whatever you want it to be so it can be referenced later on easier.  

In this example, I am going to pull the information from the config file.  
Then I am going to just output the values to the console as a test.  
This is just meant to be a basic example and template.  Hopefully to help and teach people, as well as myself.  
