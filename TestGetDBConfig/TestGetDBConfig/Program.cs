﻿using System;
using System.Configuration;

namespace TestGetDBConfig
{
    class Program
    {

        // The objective of this is just to try to get the data from a config file for the database connection.
        // The situation is where one would have a C# executable on a server that would be executed by the webpage.
        // The security standards are that we don't want the connection string in the SVN/GIT repository at all (besides these "test" ones that are dummy).
        // That also includes having the connection string in the code that will be in the repository (SVN/GIT).
        // The security standards and common sense also say we shouldn't have the config file with the connection string in a directory accessible via the web.  
        // Only something that would be accessible by the EXE.

        // The XML config file is the format provided below....
        /*
            <configuration>
	            <connectionStrings>
		            <add name="webDB" connectionString="Data Source=101.1.1.1;Initial Catalog=myDb;User ID=mySecureID;Password=password" />
	            </connectionStrings>
            </configuration>
        */
        // Change the Data Source string to what you need it, mine was changed for example purposes.
        //  change the "name="webDB"" part of the config file to whatever you want it to be so it can be referenced later on easier.

        // In this example, I am going to pull the information from the config file.
        // Then I am going to just output the values to the console as a test.
        // This is just meant to be a basic example and template.  Hopefully to help and teach people, as well as myself.
        
        static void Main(string[] args)
        {
            // set the location of the configuration file
            string configFileLocale = @"C:\GDrive\work\db.config";

            // create object for the filemap and the config 
            System.Configuration.Configuration config;
            var fileMap = new ExeConfigurationFileMap();

            // set file map to the given location of the file
            fileMap.ExeConfigFilename = configFileLocale;

            // initiate config by using the config manager and opening a mapped configuration
            config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            // do a check and see if the config object has a file 
            if (config.HasFile)
            {
                // simple Write line function show what is going on
                WriteLineOfConfig(config);

                // test getting back a string for the config - then output it to the console
                string connStr = GetConnectionString(config);
                Console.WriteLine(connStr);
            }

        }

        // adding a full example of a function to get the information
        private static string GetDBConn(string configFileLocale, string dbConnName)
        {
            // initiate the connection string to be returned
            string connString = "";

            // setup default value - if nothing is passed then use this generic location (potentially not have a file there)
            if (configFileLocale == null)
            {
                // set the location of the configuration file
                configFileLocale = @"C:\GDrive\work\db.config";
            }

            // create object for the filemap and the config 
            System.Configuration.Configuration config;
            var fileMap = new ExeConfigurationFileMap();

            // set file map to the given location of the file
            fileMap.ExeConfigFilename = configFileLocale;

            // initiate config by using the config manager and opening a mapped configuration
            config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            // do a check and see if the config object has a file 
            if (config.HasFile)
            {
                // explicitly look for the connection object and set to a setting
                ConnectionStringSettings setting1 = config.ConnectionStrings.ConnectionStrings[dbConnName];

                // the Connection String Settings object from above needs to be converted to a string
                connString = setting1.ToString();
            }
            
            // return the connection string
            return connString;
        }

        // function to get and return the connection string
        private static string GetConnectionString(System.Configuration.Configuration config)
        {
            // initiate the connection string to be returned
            string connString = "";

            // explicitly look for the connection object and set to a setting
            ConnectionStringSettings setting1 = config.ConnectionStrings.ConnectionStrings["webDB"];

            // the Connection String Settings object from above needs to be converted to a string
            connString = setting1.ToString();
            
            // return the connection string
            return connString;
        }

        // function to just write the to output the object
        private static void WriteLineOfConfig(System.Configuration.Configuration config)
        {
            // Output different things to see what we get based on the config file
            Console.WriteLine(config.ToString());                                       // Output = System.Configuration.Configuration
            Console.WriteLine(config.ConnectionStrings.ToString());                     // Output = System.Configuration.CongigurationStringsSection
            Console.WriteLine(config.ConnectionStrings.ConnectionStrings.ToString());   // Output = System.Congifuration.ConfigurationSettingsCollection

            // explicitly look for the connection object and set to a setting
            ConnectionStringSettings setting1 = config.ConnectionStrings.ConnectionStrings["webDB"];

            // Output different things to see what the "setting" object is made up of
            Console.WriteLine(setting1);                                                // Output = "Data Source=........" (without quotes)
            Console.WriteLine(setting1.ToString());                                     // Output = "Data Source=........" (without quotes)

            // loop through each setting in the config file
            foreach (ConnectionStringSettings setting in config.ConnectionStrings.ConnectionStrings)
            {
                Console.WriteLine(setting.ToString());                                  // Output = "Data Source=........" and the current data sources inside the C# program(???) (without quotes)
            }
        }
    }
}
